/*
 Copyright 2017 Aleix Pol Gonzalez <aleixpol@kde.org>

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <qqml.h>
#include <QQmlExtensionPlugin>
#include <QString>
#include <QTextStream>
#include <QFile>
#include <QFileSystemWatcher>
#include <QRegularExpression>
#include <QDebug>

class FileMonitor : public QObject
{
Q_OBJECT
Q_PROPERTY(QString lastLine READ lastLine NOTIFY lastLineChanged)
Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
public:
    FileMonitor() : QObject() {
        connect(&m_watcher, &QFileSystemWatcher::fileChanged, this, &FileMonitor::process);
    }

    QString lastLine() const { return m_lastLine; }
    QString path() const { return m_file.fileName(); }
    void setPath(QString path) {
        processPath(path);
        if (path == m_file.fileName())
            return;

        if (path.isEmpty())
            return;

        if (m_file.isOpen())
            m_watcher.removePath(m_file.fileName());
        m_file.setFileName(path);
        const auto open = m_file.open(QIODevice::ReadOnly | QIODevice::Text);
        Q_EMIT pathChanged(path);
        if (!open)
            return;

        m_stream.reset(new QTextStream(&m_file));
        process();

        m_watcher.addPath(m_file.fileName());
    }

    void process() {
        const auto lastLastLine = m_lastLine;
        QString line;
        while(m_stream->readLineInto(&line)) {
            m_lastLine = line;
        }
        if (lastLastLine != m_lastLine) {
            Q_EMIT lastLineChanged(m_lastLine);
        }
    }

Q_SIGNALS:
    void pathChanged(const QString &path);
    void lastLineChanged(const QString &lastLine);

private:
    void processPath(QString& path) {
        const QRegularExpression envRx(QStringLiteral("\\$([A-Z_]+)"));
        auto matchIt = envRx.globalMatch(path);
        while(matchIt.hasNext()) {
            auto match = matchIt.next();
            path.replace(match.capturedStart(), match.capturedLength(), QString::fromUtf8(qgetenv(match.captured(1).toUtf8())));
        }
    }

    QFile m_file;
    QString m_lastLine;
    QSharedPointer<QTextStream> m_stream;
    QFileSystemWatcher m_watcher;
};

class FileMonitorPlugin : public QQmlExtensionPlugin
{
Q_OBJECT
Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")
public:
    void registerTypes(const char* uri) override {
        qmlRegisterType<FileMonitor>(uri, 1, 0, "FileMonitor");
    }
};

#include "filemonitor.moc"
